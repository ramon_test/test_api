import unittest
from unittest.mock import patch
from poke import PokemonRequest



class TestPokemonRequest(unittest.TestCase):
    
    def test_PokemonRequest(self):
        with patch('PokemonRequest.request.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'
            pok_list = PokemonRequest().get_list()
            mocked_get.assert_called_with('https://pokeapi.co/api/v2/pokemon/')
            self.assertEqual(pok_list,'Success')







if __name__=='__main__':
    unittest.main()