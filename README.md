#Consulta de Pokemones

##API REST para buscar Pokemones - Test Python Ramon B. Ll.

###GET / ->listado de los pokemons. Incluyendo nombre parcial
###Parametros
*  q=busqueda de pokemons
*  sort=asc / desc (ordenar por nombre)
*  offset
*  limit

###GET / {nombreDelPokemón}


Datos extraidos via PokeApi(https:/pokeapi.co/)



##Instrucciones de Uso
####(el archivo requirements.txt contiene las librerías utilizadas)
####Para realizar una consulta, correr el script __init__.py de la siguiente manera:
####python3 __init__.py
####Luego ir al navegador y escribir:
####localhost:5000/
####Y visualizara este mismo archivo que esta leyendo
####Si luego escribe localhost:5000/pokemons
####Vera la lista de los pokemons.
####Para parametrizar la busqueda puede agregar limit y offset del siguiente modo:
####Si luego escribe localhost:5000/pokemons/limit=20/offset=10   
####o sino localhost:5000/pokemons/limit=5/offset=0
####Deben estar ambos parametros, en ese orden.
####Para agregar la opcion de ordenar los resultados, debe agregar luego de limit y offset respectivamente, sort=<modo>,
####siendo el modo asc (ascending) o desc (descending).


####Para realizar la consulta de un solo pokemon y su informacion debe ingresar
####o sino localhost:5000/pokemons/<nombre_del_pokemon>  , por ejemplo:
####localhost:5000/pokemons/balbasul
####Aunque el nombre no sea exacto, se buscara el nombre por similitud, siempre y cuando no este por debajo de cierto umbral,
####en cuyo caso la leyenda sera : "The name wasn't recognized: Please try with another"

###Consulta de opciones de nombres:
####Si lo que quiere es consultar que opciones de nombre de Pokemons contienten cierta sílaba o grupo de letras especifico,
####puede hacerlo escribiendo localhost:5000/pokemons/<input>&opciones, por ejemplo:
####_localhost:5000/pokemons/pika&opciones_
####Y el resultado sera:
####"The options that contain pika are ['pikachu', 'pikachu-rock-star', 'pikachu-belle', 'pikachu-pop-star', 'pikachu-phd', 'pikachu-libre', 'pikachu-cosplay', 'pikachu-original-cap', 'pikachu-hoenn-cap', 'pikachu-####sinnoh-cap', 'pikachu-unova-cap', 'pikachu-kalos-cap', 'pikachu-alola-cap', 'pikachu-partner-cap']"
