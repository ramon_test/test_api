#import de librerias necesarias
import requests
import json
from fuzzywuzzy import fuzz
import operator




#funcion para ser usada en el objeto principal, en el metodo get_info para realizar el match de nombres por probabilidad
def check_name(_input,options=False):
    URL = 'https://pokeapi.co/api/v2/pokemon?limit=1000'
    r = requests.get(URL)
    poke_result = r.content
    poke_dict_names = json.loads(poke_result)
    #names = [item['name'] for item in poke_dict_names] 
    names = [item['name'] for item in poke_dict_names['results']] 
    name_sugestion = [name for name in names if fuzz.token_set_ratio(_input,name)>75 or fuzz.ratio(_input,name)>70]
    name_options = [name for name in names if fuzz.token_set_ratio(_input,name)>80 or fuzz.partial_ratio(_input,name)>78]
    if options==False:
        if len(name_sugestion)>1:
            name_sugestion_dict = {name_finalist:(fuzz.ratio(_input,name_finalist)) for name_finalist in name_sugestion}
            name_sugestion_ok = max(name_sugestion_dict.items(), key=operator.itemgetter(1))[0]
            return name_sugestion_ok
        elif len(name_sugestion)==1:
            return name_sugestion[0]
        else:
            return "(!)...No match was found"
    else:
        if len(name_options)>0:
            return name_options
        else:
            return "(!)...No match was found"
#funcion para ser usada en __init__ para ordenar el resultado en caso que se pida
def sort_result(items,mode):
    if mode=='asc':
        newlist = sorted(items, key=lambda k: k['name']) 
        return newlist
    elif mode=='desc':
        newlist = sorted(items, key=lambda k: k['name'],reverse=True) 
        return newlist



#creo el objeto que busca los datos la api de pokemon
class PokemonRequest():
    def __init__(self):
        self.base = 'https://pokeapi.co/api/v2/pokemon/'
    
    def prepare_answer(self,api_answer_dict,mode):
        if mode==1:
            answer_dict_1 = {'total':None,'limit':None,'offset':None,'data':None}
            names = [item['name'] for item in api_answer_dict['results']]
            ids = [item['url'].split('/')[-2] for item in api_answer_dict['results']]
            images = ['https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{}.png'.format(_id) for _id in ids]
            data = []
            for _id,name,img in zip(ids,names,images):
                dict_temp = {'id':_id,'name':name,'image':img}
                data.append(dict_temp)
            answer_dict_1['data']=data
            return answer_dict_1
        elif mode==2:
            answer_dict_2 = {'abilities':None,'base_experience':None,
                'forms':None,'height':None,'id':None,
                'location_area_encounters':None,'moves':None,
                'name':None,'order':None,'species':None,
                'sprites':None,'stats':None,'types':None,'weight':None}
            abilities_raw = api_answer_dict['abilities']
            answer_dict_2['abilities'] = [abilities_raw[i]['ability']['name'] for i in range(len(abilities_raw))] 
            answer_dict_2['base_experience'] = api_answer_dict['base_experience']
            answer_dict_2['forms'] = api_answer_dict['forms'][0]['name']
            answer_dict_2['height'] = api_answer_dict['height']
            answer_dict_2['id'] = api_answer_dict['id']
            answer_dict_2['location_area_encounters'] = api_answer_dict['location_area_encounters']
            moves_json = api_answer_dict['moves']
            answer_dict_2['moves'] = [elem['move']['name'] for elem in moves_json] 
            answer_dict_2['name'] = api_answer_dict['name']
            answer_dict_2['order'] = api_answer_dict['order']
            answer_dict_2['species'] = api_answer_dict['species']['name']   
            answer_dict_2['sprites'] = api_answer_dict['sprites']
            stats_raw = api_answer_dict['stats']
            stat_names = [stats_raw[i]['stat']['name'] for i in range(len(stats_raw))] 
            stat_base = [stats_raw[i]['base_stat'] for i in range(len(stats_raw))]  
            answer_dict_2['stats'] = {k:v for k,v in zip(stat_names,stat_base)}
            answer_dict_2['types'] = api_answer_dict['types']
            answer_dict_2['weight'] = api_answer_dict['weight']
            return answer_dict_2

    def get_list(self, limit=None,offset=None):
    #metodo para extraer los nombres , con posibilidad de limit y offset
        url = self.base 
        
        if offset:
            url = url + "?offset={}".format(offset)
            print ("=======================",offset)
        elif limit:
            url = url + "?limit={}".format(limit)
        elif limit and offset:
            url = url + "?limit={}&offset={}".format(limit,offset)

        r = requests.get(url)
        prepared_dict = self.prepare_answer(json.loads(r.content),1)
        prepared_dict['total']=json.loads(r.content)['count']
        if limit is None:
            prepared_dict['limit']=20
        else:
            prepared_dict['limit']=limit 
        if offset is None:
            prepared_dict['offset']=0
        else:           
            prepared_dict['offset']=limit
        return prepared_dict

    def get_info(self,name,options=False):
    #metodo para extraer la informacion de un solo pokemon
        if options==False:
            try:
                name_checked = check_name(name)
                url = self.base + name_checked
                r = requests.get(url)
                prepared_dict = self.prepare_answer(json.loads(r.content),2)
                #return json.loads(r.content)#prepared_dict
                return prepared_dict
            except:
                return ["The name {} wasn't recognized: Please be more specific: {}".format(name,name_checked)]
        else:
            name_checked = check_name(name,options=True)
            return ["The options that contain {} are {}".format(name,name_checked)]

