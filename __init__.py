#import de librerias
import markdown
import os
import requests
# Import framework flask
from flask import Flask, g,jsonify
from flask_restful import Resource, Api, reqparse
#import de modulo propio
from poke import PokemonRequest, sort_result






#creo la instancia de applicacion Flask
app = Flask(__name__)

#Creo la instancia de la api
api = Api(app)


#decorador que crea el index. Con el readme visualizado con markdown
@app.route("/")
def index():
    with open(os.path.dirname(app.root_path) + '/pokeapi/README.md', 'r') as markdown_file:
        content = markdown_file.read()
        return markdown.markdown(content)


#Objetos para cada url-query de la api
class Pokemons(Resource):
    def get(self):
        pk = PokemonRequest()
        contenido_request = pk.get_list()
        return contenido_request

class PokemonsLimitedOffsetted(Resource):
    def get(self,limit,offset):
        pk = PokemonRequest()
        contenido_request = pk.get_list(limit=limit,offset=offset)
        return contenido_request

class PokemonsLimitedOffsettedSorted(Resource):
    def get(self,limit,offset,sort_mode):
        pk = PokemonRequest()
        contenido_request = pk.get_list(limit=limit,offset=offset)
        contenido_request_sorted = sort_result(contenido_request['results'],sort_mode)
        return contenido_request_sorted

class PokemoName(Resource):
    def get(self,pname):
        pk = PokemonRequest()
        contenido_request = pk.get_info(pname)
        return contenido_request

class PokemoNameOps(Resource):
    def get(self,pname,options):
        pk = PokemonRequest()
        art_ops = options.lower()
        if art_ops=='options':
            try:
                contenido_request = pk.get_info(pname,options=True)
                return contenido_request
            except:
                return 'ERROR'
#expresion de las url-queries y su respectivo objeto a ser llamado
api.add_resource(Pokemons,"/pokemons")
api.add_resource(PokemonsLimitedOffsetted,"/pokemons/limit=<int:limit>/offset=<int:offset>")
api.add_resource(PokemonsLimitedOffsettedSorted,"/pokemons/limit=<int:limit>/offset=<int:offset>/sort=<string:sort_mode>")
api.add_resource(PokemoName,"/pokemons/<string:pname>")
api.add_resource(PokemoNameOps,"/pokemons/<string:pname>&<string:options>")

#ejecucion
if __name__=='__main__':
    app.run(port=5000,debug=True)


